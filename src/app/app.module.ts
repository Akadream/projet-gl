import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GrainsComponent } from './grains/grains.component';
import { GrainContainerComponent } from './grain-container/grain-container.component';
import { GrainListComponent } from './grain-container/grain-list/grain-list.component';
import { GrainTreatmentDirective } from './shared/directive/grain-treatment.directive';
import { SilosListComponent } from './grain-container/silos-list/silos-list.component';
import { TreatedGrainsComponent } from './grain-container/treated-grains/treated-grains.component';
import { BuyersListComponent } from './grain-container/buyers-list/buyers-list.component';
import { SilotComponent } from './silot/silot.component';
import { SilosTemperatureDirective } from './shared/directive/silos-temperature.directive';
import {GrainService} from './shared/services/grain.service';
import {SilotService} from './shared/services/silot.service';
import {BuyerService} from './shared/services/buyer.service';

@NgModule({
  declarations: [
    AppComponent,
    GrainsComponent,
    GrainContainerComponent,
    GrainListComponent,
    GrainTreatmentDirective,
    SilosListComponent,
    TreatedGrainsComponent,
    BuyersListComponent,
    SilotComponent,
    SilosTemperatureDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [GrainService, SilotService, BuyerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
