export class Grain {

  constructor(
    public name: string,
    public weight: number,
    public size: number,
    public treated: boolean = false,
    public inSilos: boolean = false,
  ) {
  }

}
