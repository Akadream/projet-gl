import {Grain} from './grain.model';

export class Silot {

  constructor(
    public id: number,
    public grain: Grain,
    public temperature: number,
    public seleable: boolean = false,
  ) {
  }

  defective(): boolean {
    return this.temperature > 40;
  }

}
