import {Grain} from './grain.model';

export class Buyer {

  constructor(
    public name: string,
    public favorite: string
  ) {
  }

  offer(grain: Grain): number {
    if (grain.name === this.favorite) {
      return grain.size * grain.weight * 1.2;
    }

    return grain.size * 1.2;
  }

}
