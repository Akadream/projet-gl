import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Silot} from '../models/silot.model';

@Injectable()
export class SilotService {

  public silots: BehaviorSubject<Silot[]> = new BehaviorSubject([
    new Silot(0, null, 20),
    new Silot(1, null, 20),
    new Silot(2, null, 20),
    new Silot(3, null, 20),
    new Silot(4, null, 20),
    new Silot(5, null, 20),
    new Silot(6, null, 20),
    new Silot(7, null, 20),
  ]);
// tslint:disable-next-line:variable-name
  public _silot: BehaviorSubject<Silot> = new BehaviorSubject(this.silots.value[0]);
  public silot: Observable<Silot> = this._silot.asObservable();

  selectSilot(index: number): void {
    this._silot.next(this.silots.value[index]);
  }

  constructor() { }
}
