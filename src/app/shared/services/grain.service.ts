import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Grain} from '../models/grain.model';

@Injectable()
export class GrainService {
  // private grainTimer = 30000;
  private grainTimer = 10000;
  private grainsName: string[] = ['Blé tendre', 'Maïs', 'Orge', 'Blé dur', 'Triticale', 'Avoine', 'Riz', 'Sorgho', 'Seigle', 'Épeautre', 'Millet', 'Quinoa', 'Sarrasin'];

  public grains: BehaviorSubject<Grain[]> = new BehaviorSubject([
  ]);

  public grain: BehaviorSubject<Grain> = new BehaviorSubject(null);
  // tslint:disable-next-line:variable-name
  public _selectedIndex: BehaviorSubject<number> = new BehaviorSubject(-1);

  public selectedIndex: Observable<number> = this._selectedIndex.asObservable();

  selectGrain(index: number): void {
    this._selectedIndex.next(index);
    if (index >= 0) { this.grain.next(this.grains.value[index]); }
    else { this.grain.next(null); }
  }

  private getRandomName(): string {
    const r = Math.round(Math.random() * ((this.grainsName.length - 1)));
    return this.grainsName[r];
  }

  removeGrain(index: number): void {
    if (index >= 0) {
      this.grains.next(this.grains.value.filter((v, i) => index !== i));
      this.selectGrain(-1);
    }
  }

  constructor() {
    this.generate();
    setInterval(() => {
      if (this.grains.value.length < 8) {
        this.generate();
      }
    }, this.grainTimer);
  }

  private generate(): void
  {
    const name = this.getRandomName();
    const weight = Math.round(Math.random() * (5 - 1) + 1);
    const size = Math.round(Math.random() * (700 - 200) + 200);

    this.grains.value.push(new Grain(name, weight, size));
  }
}
