import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { GrainService } from './grain.service';
import {Grain} from '../models/grain.model';

describe('GrainService', () => {
  let service: GrainService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [GrainService] });
    service = TestBed.inject(GrainService);
    service.grains.value.push(new Grain('Test', 1, 1));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should select grain', () => {
    const index = 1;
    service.selectGrain(index);
    expect(service._selectedIndex.value).toBe(index);
  });

  it('should remove grain', () => {
    const index = 0;
    const length = service.grains.value.length;
    service.removeGrain(index);
    expect(service.grains.value.length).toBe(length - 1);
  });
});
