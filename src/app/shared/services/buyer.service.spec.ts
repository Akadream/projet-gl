import { TestBed } from '@angular/core/testing';

import { BuyerService } from './buyer.service';

describe('BuyerService', () => {
  let service: BuyerService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [BuyerService] });
    service = TestBed.inject(BuyerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update buyer', () => {
    const index = 1;
    service.updateBuyer(index);
    expect(service.buyers.value[index]).toBeDefined();
  });
});
