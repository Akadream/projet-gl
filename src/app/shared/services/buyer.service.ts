import { Injectable } from '@angular/core';
import {Buyer} from '../models/buyer.model';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class BuyerService {

  private grainsName: string[] = ['Blé tendre', 'Maïs', 'Orge', 'Blé dur', 'Triticale', 'Avoine', 'Riz', 'Sorgho', 'Seigle', 'Épeautre', 'Millet', 'Quinoa', 'Sarrasin'];

  public buyers: BehaviorSubject<Buyer[]> = new BehaviorSubject([
    new Buyer('Aptiva', 'Maïs'),
    new Buyer('Fadec', 'Orge'),
    new Buyer('Feuerstein', 'Avoine'),
    new Buyer('Nestlé', 'Blé tendre'),
    new Buyer('Test', 'Sorgho'),
    new Buyer('Bonjour', 'Épeautre'),
  ]);

  updateBuyer(index: number): void {
    this.buyers.value[index].favorite = this.getRandomName();
  }

  private getRandomName(): string {
    const r = Math.round(Math.random() * ((this.grainsName.length - 1)));
    return this.grainsName[r];
  }

  constructor() { }
}
