import { TestBed } from '@angular/core/testing';

import { SilotService } from './silot.service';

describe('SilotService', () => {
  let service: SilotService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [SilotService] });
    service = TestBed.inject(SilotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should select silo', () => {
    const index = 1;
    service.selectSilot(index);
    expect(service._silot.value).toBe(service.silots.value[index]);
  });
});
