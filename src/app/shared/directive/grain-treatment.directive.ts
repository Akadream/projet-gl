import {Directive, ElementRef} from '@angular/core';
import {GrainService} from '../services/grain.service';

@Directive({
  selector: '[appGrainTreatment]'
})
export class GrainTreatmentDirective {

  // public weighingDuration = 5;
  // public hopperDuration = 12;
  // public sampleCollectionDuration = 21;
  // public elevatorDuration = 31;
  // public cleaning = 42;

  // TODO : Update
  public weighingDuration = 2;
  public hopperDuration = 5;
  public sampleCollectionDuration = 8;
  public elevatorDuration = 12;
  public cleaning = 16;

  private currentGrain: number;

  constructor(el: ElementRef, grainService: GrainService) {
    let progress = 0;
    let seconds = 0;
    let ms = 0;
    const maximumDuration = 50;

    grainService.selectedIndex.subscribe((index: number) => {
      this.currentGrain = index;
    });

    const id = setInterval(async () => {
      if (this.currentGrain >= 0 && !grainService.grains.value[this.currentGrain].treated) {
        if (seconds >= maximumDuration) {
          clearInterval(id);
          return;
        }

        ms += 50;
        seconds = Math.floor(ms / 1000);

        if (seconds >= 0 && seconds < this.weighingDuration) {
          if (progress <= 5) { progress++; }
        }
        else if (seconds >= this.weighingDuration && seconds < this.hopperDuration)
        {
          if (progress <= 20) { progress++; }
        }
        else if (seconds >= this.hopperDuration && seconds < this.sampleCollectionDuration)
        {
          if (progress <= 35) { progress++; }
        }
        else if (seconds >= this.sampleCollectionDuration && seconds < this.elevatorDuration)
        {
          if (progress <= 55) { progress++; }
        }
        else if (seconds >= this.elevatorDuration && seconds < this.cleaning)
        {
          if (progress <= 88) { progress++; }
        }
        else {
          if (progress < 100) { progress++; }
          else {
            grainService.grain.value.treated = true;
            grainService.selectGrain(-1);

            progress = 0;
            ms = 0;
            seconds = 0;

            document.getElementById('grain-view').style.display = 'none';
            document.getElementById('main-view').style.display = 'block';
          }
        }

        el.nativeElement.style.width = `${progress}%`;
      }
    }, 50);
  }
}
