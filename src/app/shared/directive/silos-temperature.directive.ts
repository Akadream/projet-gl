import {Directive, Input} from '@angular/core';
import {Silot} from '../models/silot.model';

@Directive({
  selector: '[appSilosTemperature]'
})
export class SilosTemperatureDirective {

  @Input('appSilosTemperature') silot: Silot;
  private count = 0;

  constructor() {
    setInterval(() => {
      if (this.silot && this.silot.grain && !this.silot.seleable) {
        // const temperature = Math.round(Math.random() * (1 - (-1)) + (-1));
        const temperature = Math.round(Math.random() * (1 - (0)) + (0));
        // tslint:disable-next-line:max-line-length
        if (temperature !== -1 || (this.silot.temperature !== 30 && this.silot.temperature !== 40)) { this.silot.temperature += temperature; }
        if (this.silot.temperature < 18) { this.silot.temperature = 18; }

        if (this.silot.temperature < 30) { this.count++; }
        if (this.count >= 30) { this.silot.seleable = true; this.count = 0; }
      }
    }, 1000);
  }
}
