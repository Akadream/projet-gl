import { GrainTreatmentDirective } from './grain-treatment.directive';
import {ElementRef} from '@angular/core';
import {GrainService} from '../services/grain.service';

describe('GrainTreatmentDirective', () => {
  let grainService: GrainService;
  let elementRef: ElementRef;

  it('should create an instance', () => {
    grainService = new GrainService();
    elementRef = new ElementRef<any>({ style: { width: 0 } });

    const directive = new GrainTreatmentDirective(elementRef, grainService);
    expect(directive).toBeTruthy();
  });
});
