import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrainsComponent } from './grains.component';
import {GrainService} from '../shared/services/grain.service';

describe('GrainsComponent', () => {
  let component: GrainsComponent;
  let fixture: ComponentFixture<GrainsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrainsComponent ],
      providers: [ GrainService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
