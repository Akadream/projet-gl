import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Grain} from '../shared/models/grain.model';
import {GrainService} from '../shared/services/grain.service';

@Component({
  selector: 'app-grains',
  templateUrl: './grains.component.html',
  styleUrls: ['./grains.component.scss']
})
export class GrainsComponent implements OnInit {

  @Output() grainEvent = new EventEmitter<any>();

  grains: Grain[];
  public grain: Grain;
  public currentGrain = -1;

  constructor(private grainService: GrainService) {
    this.grainService.grains.subscribe((grains: Grain[]) => {
      this.grains = grains;
    });

    this.grainService.grain.subscribe((grain: Grain) => {
      this.grain = grain;
    });

    this.grainService.selectedIndex.subscribe((index: number) => {
      this.currentGrain = index;
    });
  }

  ngOnInit(): void {
    this.grain = this.grainService.grain.value;
  }

}
