import { Component, OnInit } from '@angular/core';
import {Shape} from '../shared/models/shape.model';
import {Grain} from '../shared/models/grain.model';
import {SilotService} from '../shared/services/silot.service';
import {Silot} from '../shared/models/silot.model';

@Component({
  selector: 'app-silot',
  templateUrl: './silot.component.html',
  styleUrls: ['./silot.component.scss']
})
export class SilotComponent implements OnInit {
  public shapes: Shape[] = [];
  public grain: Grain;
  public silot: Silot;

  constructor(private silotService: SilotService) {
    silotService.silot.subscribe((silot: Silot) => {
      this.silot = silot;
    });

    setInterval(() => {
      if (this.shapes.length === 0 && this.silot) {
        const shapeCount = Math.round(Math.random() * (8 - 5) + 5);
        for (let i = 0; i < shapeCount; i++) {
          const top = Math.random() * (99 - 1) + 1;
          const left = Math.random() * (99 - 1) + 1;

          const shape = new Shape(top, left);
          this.shapes.push(shape);
        }
      }
    }, 10);
  }

  ngOnInit(): void {

  }

  remove(shape: Shape): void {
    this.shapes = this.shapes.filter(item => item !== shape);

    if (this.shapes.length <= 0) {
      // Terminé
      this.silot.temperature -= 10;

      document.getElementById('silot-view').style.display = 'none';
      document.getElementById('main-view').style.display = 'block';

      this.silotService.selectSilot(-1);
    }
  }

}
