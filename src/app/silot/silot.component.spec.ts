import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SilotComponent } from './silot.component';
import {SilotService} from '../shared/services/silot.service';
import {Shape} from '../shared/models/shape.model';

describe('SilotComponent', () => {
  let component: SilotComponent;
  let fixture: ComponentFixture<SilotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SilotComponent ],
      providers: [ SilotService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SilotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should remove shape', () => {
  //   component.shapes.push(new Shape(12, 13));
  //   fixture.detectChanges();
  //   const testLength = component.shapes.length;
  //   component.remove(component.shapes[0]);
  //   expect(component.shapes.length).toBe(testLength - 1);
  // });
});
