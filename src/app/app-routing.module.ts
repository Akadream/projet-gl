import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GrainContainerComponent} from './grain-container/grain-container.component';
import {GrainsComponent} from './grains/grains.component';
import {SilotComponent} from './silot/silot.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
