import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {NavigationStart, Router} from '@angular/router';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {
  title = 'projet-gl';

  constructor(public router: Router) {
  }

  ngOnInit(): void {}
}
