import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrainContainerComponent } from './grain-container.component';

describe('GrainContainerComponent', () => {
  let component: GrainContainerComponent;
  let fixture: ComponentFixture<GrainContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrainContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrainContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
