import { Component, OnInit } from '@angular/core';
import {Buyer} from '../../shared/models/buyer.model';
import {Silot} from '../../shared/models/silot.model';
import {BuyerService} from '../../shared/services/buyer.service';
import {SilotService} from '../../shared/services/silot.service';
import {GrainService} from '../../shared/services/grain.service';

@Component({
  selector: 'app-buyers-list',
  templateUrl: './buyers-list.component.html',
  styleUrls: ['./buyers-list.component.scss']
})
export class BuyersListComponent implements OnInit {

  public buyers: Buyer[];
  public silots: Silot[];
  public currentSilot: Silot = null;
  public selectedGrain = -1;

  constructor(private buyerService: BuyerService, private silotService: SilotService, private grainService: GrainService) {
    buyerService.buyers.subscribe((buyers: Buyer[]) => {
      this.buyers = buyers;
    });

    silotService.silots.subscribe((silots: Silot[]) => {
      this.silots = silots;
    });

    silotService.silot.subscribe((silot: Silot) => {
      this.currentSilot = silot;
    });

    grainService.selectedIndex.subscribe((index: number) => {
      this.selectedGrain = index;
    });
  }

  dropSilot(buyerIndex: number): void {
    if (buyerIndex < 0) { return; }
    if (this.currentSilot && this.currentSilot.seleable && this.currentSilot.grain.name === this.buyers[buyerIndex].favorite) {
      this.currentSilot.grain = null;
      this.currentSilot.seleable = false;
      this.grainService.removeGrain(this.selectedGrain);
      this.buyerService.updateBuyer(buyerIndex);
    }
  }

  dragOver(event): void {
    event.preventDefault();
  }

  ngOnInit(): void {
  }

}
