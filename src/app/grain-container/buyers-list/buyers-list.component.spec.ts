import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyersListComponent } from './buyers-list.component';
import {GrainService} from '../../shared/services/grain.service';
import {SilotService} from '../../shared/services/silot.service';
import {BuyerService} from '../../shared/services/buyer.service';

describe('BuyersListComponent', () => {
  let component: BuyersListComponent;
  let fixture: ComponentFixture<BuyersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyersListComponent ],
      providers: [ GrainService, SilotService, BuyerService ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
