import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Grain} from '../../shared/models/grain.model';
import {GrainService} from '../../shared/services/grain.service';

@Component({
  selector: 'app-grain-list',
  templateUrl: './grain-list.component.html',
  styleUrls: ['./grain-list.component.scss']
})
export class GrainListComponent implements OnInit {
  public grains: Grain[];
  public currentGrain = -1;

  @Output() treatmentEvent = new EventEmitter<number>();

  constructor(private grainService: GrainService) {
    this.grainService.grains.subscribe((grains: Grain[]) => {
      this.grains = grains;
    });

    this.grainService.selectedIndex.subscribe((index: number) => {
      this.currentGrain = index;
    });
  }

  getGrain(index: number): void {
    this.grainService.selectGrain(index);

    document.getElementById('main-view').style.display = 'none';
    document.getElementById('grain-view').style.display = 'block';

    this.treatmentEvent.emit(index);
  }

  ngOnInit(): void {
  }

}
