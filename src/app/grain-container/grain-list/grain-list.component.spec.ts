import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrainListComponent } from './grain-list.component';
import {GrainService} from '../../shared/services/grain.service';

describe('GrainListComponent', () => {
  let component: GrainListComponent;
  let fixture: ComponentFixture<GrainListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrainListComponent ],
      providers: [ GrainService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrainListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
