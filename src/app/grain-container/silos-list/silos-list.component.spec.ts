import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SilosListComponent } from './silos-list.component';
import {SilotService} from '../../shared/services/silot.service';
import {GrainService} from '../../shared/services/grain.service';

describe('SilosListComponent', () => {
  let component: SilosListComponent;
  let fixture: ComponentFixture<SilosListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SilosListComponent ],
      providers: [ SilotService, GrainService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SilosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
