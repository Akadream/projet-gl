import { Component, OnInit } from '@angular/core';
import {Silot} from '../../shared/models/silot.model';
import {SilotService} from '../../shared/services/silot.service';
import {GrainService} from '../../shared/services/grain.service';
import {Grain} from '../../shared/models/grain.model';

@Component({
  selector: 'app-silos-list',
  templateUrl: './silos-list.component.html',
  styleUrls: ['./silos-list.component.scss']
})
export class SilosListComponent implements OnInit {

  public silots: Silot[];
  public silotGrains: Grain[];
  public currentSilot = -1;
  public currentGrain: number;

  constructor(private silotService: SilotService, private grainService: GrainService) {
    this.silotService.silots.subscribe((silots: Silot[]) => {
      this.silots = silots;
    });

    this.grainService.grains.subscribe((grains: Grain[]) => {
      this.silotGrains = grains;
    });

    this.grainService.selectedIndex.subscribe((index: number) => {
      this.currentGrain = index;
    });
  }

  getSilot(index: number): void {
    this.currentSilot = index;
    this.silotService.selectSilot(index);
  }

  ngOnInit(): void {
  }

  dropGrain(silotIndex: number): void {
    if (this.currentGrain >= 0 && !this.grainService.grain.value.inSilos && !this.silots[silotIndex].grain) {
      this.silots[silotIndex].grain = this.silotGrains[this.currentGrain];
      this.silots[silotIndex].grain.inSilos = true;
      this.silotService.selectSilot(-1);
    }
  }

  dragSilot(index: number): void {
    this.silotService.selectSilot(index);
  }

  clicker(index: number): void {
    if (index >= 0 && this.silots[index].temperature >= 30) {
      this.silotService.selectSilot(index);

      document.getElementById('main-view').style.display = 'none';
      document.getElementById('silot-view').style.display = 'block';
    }
  }

  dragOver(event): void {
    event.preventDefault();
  }

}
