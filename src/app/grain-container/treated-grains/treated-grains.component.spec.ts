import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatedGrainsComponent } from './treated-grains.component';
import {GrainService} from '../../shared/services/grain.service';

describe('TreatedGrainsComponent', () => {
  let component: TreatedGrainsComponent;
  let fixture: ComponentFixture<TreatedGrainsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreatedGrainsComponent ],
      providers: [ GrainService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatedGrainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
