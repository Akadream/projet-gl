import { Component, OnInit } from '@angular/core';
import {GrainService} from '../../shared/services/grain.service';
import {Grain} from '../../shared/models/grain.model';

@Component({
  selector: 'app-treated-grains',
  templateUrl: './treated-grains.component.html',
  styleUrls: ['./treated-grains.component.scss']
})
export class TreatedGrainsComponent implements OnInit {

  public treatedGrains: Grain[];
  public currentGrain = -1;

  constructor(private grainService: GrainService) {
    this.grainService.grains.subscribe((grains: Grain[]) => {
      this.treatedGrains = grains;
    });

    this.grainService.selectedIndex.subscribe((index: number) => {
      this.currentGrain = index;
    });
  }

  dragGrain(index: number): void {
    // this.currentGrain = index;
    this.grainService.selectGrain(index);
  }

  dropGrain(): void {
    this.grainService.selectGrain(-1);
  }

  ngOnInit(): void {
  }

}
